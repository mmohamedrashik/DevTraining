package imdroidguy.com.devtraining.JobSchedulerTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;

public class JobBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        JobUtil.scheduleJob(context);
    }
}
