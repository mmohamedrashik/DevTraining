package imdroidguy.com.devtraining.JobSchedulerTask;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;


public class JobSchedulerTask extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Toast.makeText(this, "JOB", Toast.LENGTH_SHORT).show();
        JobUtil.scheduleJob(getApplicationContext());
        Log.e("JOB","JOB");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}
