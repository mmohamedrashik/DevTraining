package imdroidguy.com.devtraining.CustomViewActivity;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import imdroidguy.com.devtraining.R;


public class CustomView extends RelativeLayout implements View.OnClickListener{
    View mRootview;
    EditText valueTextView;
    Button btnIncrease;
    int i = 0;
    public CustomView(Context context) {
        super(context);
        initView(context,null);
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context,attrs);
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context,attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context,attrs);
    }

    void initView(Context context, AttributeSet attrs){
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.CustomView, 0, 0);
        try{
            i = ta.getInteger(R.styleable.CustomView_initial_value,0);
        }catch (Exception e){

        }
        mRootview = inflate(context, R.layout.custom_button, this);
        btnIncrease = mRootview.findViewById(R.id.btnIncrease);
        valueTextView = mRootview.findViewById(R.id.valueTextView);
        btnIncrease.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        i++;
        valueTextView.setText(String.valueOf(i));
    }
}
