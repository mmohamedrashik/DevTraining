package imdroidguy.com.devtraining.ArchitectureComponents.LiveDataActivity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class LViewModel extends ViewModel {
    public MutableLiveData<String> mUserName= new MutableLiveData<>();
    public LiveData<String> getUserName(){
        if (mUserName == null){
            mUserName = new MutableLiveData<>();
        }
        return mUserName;
    }

    public Integer  i;

    public int getI() {
        if (i == null){
            i=0;
        }
        return i++;
    }

    public void setI(int i) {
        this.i = i;
    }
}
