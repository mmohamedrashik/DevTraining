package imdroidguy.com.devtraining.ArchitectureComponents.MyLifeCycleEvents;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.util.Log;

public class MyObserver implements LifecycleObserver {
    private String TAG = "OBSERVER";

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    public void created(){
        Log.i(TAG,"CREATED");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void connected(){
        Log.i(TAG,"START");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void start(){
        Log.i(TAG,"STARTED");
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void disconnectListener() {
        Log.i(TAG,"DISCONNECTED");
    }

}
