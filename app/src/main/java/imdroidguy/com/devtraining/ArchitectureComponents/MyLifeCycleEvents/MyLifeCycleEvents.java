package imdroidguy.com.devtraining.ArchitectureComponents.MyLifeCycleEvents;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleRegistry;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import imdroidguy.com.devtraining.R;
import imdroidguy.com.devtraining.databinding.ActivityMyLifeCycleEventsBinding;

public class MyLifeCycleEvents extends AppCompatActivity {
    private LifecycleRegistry mLifecycleRegistry;
    ActivityMyLifeCycleEventsBinding binding;
    MyObserver observer = new MyObserver();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_my_life_cycle_events);
        binding.setLifecycleOwner(this);
        mLifecycleRegistry = new LifecycleRegistry(this);
        mLifecycleRegistry.addObserver(observer);
        mLifecycleRegistry.markState(Lifecycle.State.CREATED);

    }

    @Override
    protected void onStart() {
        super.onStart();
        mLifecycleRegistry.markState(Lifecycle.State.STARTED);
    }
}