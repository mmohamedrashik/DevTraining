package imdroidguy.com.devtraining.ArchitectureComponents.RoomActivity;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

public class RoomViewModel extends ViewModel{
    private   UserDao userDao;
    public LiveData<PagedList<User>> userList;

    public RoomViewModel() {
    }

    public RoomViewModel(UserDao userDao) {
        this.userDao = userDao;
        userList = new LivePagedListBuilder<>(this.userDao.listAll(),3).build();
    }
}
