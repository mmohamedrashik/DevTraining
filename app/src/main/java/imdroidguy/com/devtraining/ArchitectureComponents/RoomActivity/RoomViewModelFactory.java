package imdroidguy.com.devtraining.ArchitectureComponents.RoomActivity;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

public class RoomViewModelFactory extends ViewModelProvider.NewInstanceFactory {
   private UserDao mUserDao;

    public RoomViewModelFactory(UserDao mUserDao) {
        this.mUserDao = mUserDao;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RoomViewModel(mUserDao);
    }
}
