package imdroidguy.com.devtraining.ArchitectureComponents.DataBindingActivity;

import android.databinding.DataBindingUtil;
import android.databinding.Observable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;


import imdroidguy.com.devtraining.R;
import imdroidguy.com.devtraining.databinding.ActivityDataBindingBinding;

public class DataBindingActivity extends AppCompatActivity implements DataBinidingObservable.Listener{

    ActivityDataBindingBinding bindingBinding;
    DataBinidingObservable dataBinidingObservable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataBinidingObservable = new DataBinidingObservable();
        dataBinidingObservable.setProperty(this);
        bindingBinding = DataBindingUtil.setContentView(this,R.layout.activity_data_binding);
        bindingBinding.setObs(dataBinidingObservable);

        bindingBinding.btnSetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mApps = bindingBinding.edtTextName.getText().toString();
                dataBinidingObservable.setmAppTitle(mApps);
                dataBinidingObservable.mObservable.set(mApps);
            }
        });




    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onDataSetChanged(int fieldId) {
        Toast.makeText(this, ""+fieldId, Toast.LENGTH_SHORT).show();
    }
}
