package imdroidguy.com.devtraining.ArchitectureComponents.RoomActivity;

import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.databinding.library.baseAdapters.BR;

import imdroidguy.com.devtraining.databinding.RecyclerUserBinding;

public class UserAdapter extends PagedListAdapter<User,UserAdapter.UserViewHolder> {

    public UserAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());
        RecyclerUserBinding recyclerUserBinding = RecyclerUserBinding.inflate(layoutInflater,parent,false);
        return new UserViewHolder(recyclerUserBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = getItem(position);

        if (user != null) {
            holder.bind(user);
        }
    }

    private static DiffUtil.ItemCallback<User> DIFF_CALLBACK = new DiffUtil.ItemCallback<User>() {
        @Override
        public boolean areItemsTheSame(User oldItem, User newItem) {
            return oldItem.getUid() == newItem.getUid();
        }

        @Override
        public boolean areContentsTheSame(User oldItem, User newItem) {
            return oldItem.equals(newItem);
        }
    };
    class UserViewHolder extends RecyclerView.ViewHolder{
        RecyclerUserBinding binding;
        public UserViewHolder(RecyclerUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


        public void bind(User user) {
            binding.setVariable(BR.user, user);
            binding.executePendingBindings();
        }
    }
}
