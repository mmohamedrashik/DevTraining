package imdroidguy.com.devtraining.ArchitectureComponents.RoomActivity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.arch.persistence.room.Room;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.databinding.library.baseAdapters.BR;

import java.util.Iterator;
import java.util.List;

import imdroidguy.com.devtraining.DevTrainingApplication;
import imdroidguy.com.devtraining.R;
import imdroidguy.com.devtraining.databinding.ActivityRoomBinding;

public class RoomActivity extends AppCompatActivity {

    String TAG = "Room";
    ActivityRoomBinding binding;
    AppDatabase db;
    ListUsers listUsers;
    InsertUsers insertUsers;
    RoomViewModel roomViewModel;

    Observer<PagedList<User>> userObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this,R.layout.activity_room);

        db = Room.databaseBuilder(this,
                AppDatabase.class, "app-name").build();

      //  roomViewModel = ViewModelProviders.of(this).get(RoomViewModel.class); // ASSIGNING VIEW MODEL WITHOUT ARGUMENT
        roomViewModel = ViewModelProviders.of(this,new RoomViewModelFactory(db.userDao())).get(RoomViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setVariable(BR.roomviewmodel,roomViewModel);
        UserAdapter userAdapter = new UserAdapter();
        binding.recyclerViewUser.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerViewUser.setAdapter(userAdapter);

        roomViewModel.userList.observe(this, new Observer<PagedList<User>>() {
                @Override
                public void onChanged(@Nullable PagedList<User> users) {
                    userAdapter.submitList(users);
                }
        });







        binding.btnGet.setOnClickListener(v ->
            getUserList()
        );

        binding.btnInsert.setOnClickListener(v -> {
            insertUsers();
        });

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUser();
            }
        });

        binding.btnDeleteAll.setOnClickListener(v ->
            deleteUserList()
        );

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void getUserList(){
        listUsers = new ListUsers();
        listUsers.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void insertUsers(){
        User user =new User();
        user.setFirstName(binding.txtFirstName.getText().toString());
        user.setLastName(binding.txtLastName.getText().toString());
        insertUsers = new InsertUsers();
        insertUsers.execute(user);
    }

    private void showUser(){
        GetUserDetails getUserDetails = new GetUserDetails();
        User user =new User();
        user.setFirstName(binding.txtFirstName.getText().toString()+"%");
        user.setLastName(binding.txtLastName.getText().toString()+"%");
        getUserDetails.execute(user);
    }
    private void userList(List<User> users){
        Log.e(TAG, String.valueOf(users.size()));
        if (users.size() !=0){
            for (User user : users){
                Log.i(TAG,user.getFirstName()+" "+user.getLastName());
            }
        }
    }

    private void userDetail(User user){
        if (user !=null)
            Log.i(TAG,user.getFirstName()+" "+user.getLastName());
    }

    private void deleteUserList(){
        Thread deleteThread = new Thread(() -> db.userDao().deleteTable());
        deleteThread.setPriority(Thread.NORM_PRIORITY);
        deleteThread.start();
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    class  InsertUsers extends AsyncTask<User,Void,Void>{
        @Override
        protected Void doInBackground(User... users) {
            db.userDao().insertAll(users);
            return null;
        }
    }

    class GetUserDetails extends AsyncTask<User,Void,User>{
        @Override
        protected User doInBackground(User... users) {
            User mUser = db.userDao().findByName(users[0].getFirstName(),users[0].getLastName());
            return mUser;
        }

        @Override
        protected void onPostExecute(User user) {
            userDetail(user);
        }
    }
    class ListUsers extends AsyncTask<Void,Void,List<User>>{

        @Override
        protected List<User> doInBackground(Void... voids) {
            List<User> userList = db.userDao().getAll();
            return userList;
        }

        @Override
        protected void onPostExecute(List<User> users) {
            userList(users);
        }
    }
}
