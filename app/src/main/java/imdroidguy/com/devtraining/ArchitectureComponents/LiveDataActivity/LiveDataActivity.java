package imdroidguy.com.devtraining.ArchitectureComponents.LiveDataActivity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import imdroidguy.com.devtraining.R;
import imdroidguy.com.devtraining.databinding.ActivityLiveDataBinding;

public class LiveDataActivity extends AppCompatActivity {

    ActivityLiveDataBinding binding;
    LViewModel lViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_live_data);
        lViewModel = ViewModelProviders.of(this).get(LViewModel.class);
        binding.setMylivedata(lViewModel);
        binding.setLifecycleOwner(this);
        binding.btnLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lViewModel.getI();
                lViewModel.mUserName.setValue(""+lViewModel.i);
            }
        });

        lViewModel.getUserName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Toast.makeText(LiveDataActivity.this, "CHANGED"+s, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
