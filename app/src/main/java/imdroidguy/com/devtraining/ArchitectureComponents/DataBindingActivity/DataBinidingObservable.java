package imdroidguy.com.devtraining.ArchitectureComponents.DataBindingActivity;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.ObservableField;
import android.databinding.PropertyChangeRegistry;
import android.support.annotation.NonNull;

import com.android.databinding.library.baseAdapters.BR;


public class DataBinidingObservable extends BaseObservable implements Observable{
    public String mAppTitle;
    public ObservableField<String> mObservable = new ObservableField<>();
    public  PropertyChangeRegistry callbacks = new PropertyChangeRegistry();
    Listener listener;

    public void setProperty(Listener listener){
        this.listener= listener;
    }
    @Bindable
    public String getmAppTitle() {
        return mAppTitle;
    }

    public void setmAppTitle(String mAppTitle) {
        this.mAppTitle = mAppTitle;
        notifyPropertyChanged(BR.mAppTitle);
    }

    @Bindable
    public ObservableField<String> getmObservable() {
        return mObservable;
    }

    public void setmObservable(ObservableField<String> mObservable) {
        this.mObservable = mObservable;
    }

    @Override
    public void notifyPropertyChanged(int fieldId) {
        super.notifyPropertyChanged(fieldId);
        listener.onDataSetChanged(fieldId);
        callbacks.notifyChange(this,fieldId);

    }

    @Override
    public void addOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        super.addOnPropertyChangedCallback(callback);
        callbacks.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        super.removeOnPropertyChangedCallback(callback);
        callbacks.remove(callback);
    }

    public interface Listener{
        void onDataSetChanged(int fieldId);
    }
}
