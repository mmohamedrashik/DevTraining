package imdroidguy.com.devtraining;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import imdroidguy.com.devtraining.ArchitectureComponents.RoomActivity.AppDatabase;

public class DevTrainingApplication extends MultiDexApplication {

    static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        context = getApplicationContext();
    }

    public static AppDatabase getDbInstance(){
        AppDatabase db = Room.databaseBuilder(context,
                AppDatabase.class, "app-name").build();
        return db;
    }
}
