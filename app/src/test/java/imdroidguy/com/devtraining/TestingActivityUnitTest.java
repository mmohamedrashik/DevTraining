package imdroidguy.com.devtraining;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestingActivityUnitTest {

    String mString = "100";
    int mVal = 100;
    @Test
    public void StringMatches(){
        int result = Integer.parseInt(mString);
        assertEquals(mVal,result);
    }

    @Test
    public void conversionSuccess(){
        String mValue = String.valueOf(mVal);
        assertEquals(mString,mValue);
    }
}
