package imdroidguy.com.devtraining;


import android.os.Parcel;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Pair;

import org.hamcrest.Matchers;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.List;
import java.util.regex.Matcher;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
@SmallTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InstrumentedUnitTest {

    public static final String TEST_STRING = "STRING_ONE";
    public static final String TEST_STRING_TWO = "";

    @Test
    public void InstrumentalTest(){
        assertThat(TEST_STRING, Matchers.is(TEST_STRING));
    }

    @Test
    public void InstrumentalTestTwo(){
        assertThat(TEST_STRING_TWO,Matchers.isEmptyString());
    }

}
