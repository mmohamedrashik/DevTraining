package imdroidguy.com.devtraining;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;


import imdroidguy.com.devtraining.TestingActivity.TestingActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestActivityUITest {
    private String mStringToBetyped;

    @Rule
    public ActivityTestRule<TestingActivity> mTestingActivity = new ActivityTestRule<>(TestingActivity.class);


    @Before
    public void initValidString() {
        // Specify a valid string.
        mStringToBetyped = "Espresso";
    }

    @Test
    public void TextViewTest(){
        // Type text and then press the button.
        onView(withId(R.id.editText))
                .perform(ViewActions.clearText());

        onView(withId(R.id.editText)).perform(ViewActions.typeText(mStringToBetyped));

        onView(withId(R.id.btnSend)).perform(ViewActions.click());
    }
}
